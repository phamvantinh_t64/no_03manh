<?php 
	class Model extends Define
	{
		
		function __construct()
		{
			parent::__construct(); 
		}  
		
		public function getScore() {
			$sql = "SELECT scores.id as id, students.name as student_name, subjects.name as subject_name, teachers.name as teacher_name, scores.score as score  FROM scores
			INNER JOIN students ON scores.student_id = students.id
			INNER JOIN subjects ON scores.subject_id = subjects.id
			INNER JOIN teachers ON scores.teacher_id = teachers.id;";
			return $this->pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
			}
		public function searchScores($student,$subject,$teacher) {
			$sql = "SELECT scores.id as id,students.name as student_name, subjects.name as subject_name, teachers.name as teacher_name, scores.score as score  FROM scores
			INNER JOIN students ON scores.student_id = students.id
			INNER JOIN subjects ON scores.subject_id = subjects.id
			INNER JOIN teachers ON scores.teacher_id = teachers.id
			WHERE  students.name LIKE '%$student%' 
			AND subjects.name LIKE '%$subject%' 
			AND teachers.name LIKE '%$teacher%'
			ORDER BY scores.id DESC;";
			return $this->pdo->query($sql)->fetchAll(PDO::FETCH_ASSOC);
			}
		
		public function deleteScore($id){
			$sql = "DELETE FROM `scores` WHERE `id` =:id";
			$pre = $this->pdo->prepare($sql);
			$pre->bindParam(':id', $id);
			$id = (int)$id;
			$pre->execute();
		}
	
	}
?>