<?php 
	class Controller extends Model
	{
		private $model;
		function __construct()
		{
			$this->model = new Model(); 
		}
		

		public function Controllers()
		{
			if (isset($_GET['page'])) {
                $page = $_GET['page'];
            }else {
                $page = "home";
            }

            switch ($page) {
            	case 'home':
            		include_once "./app/view/".$page.".php";
					break;
				case 'score_search':
					if (isset($_GET['method'])) {
		                $method = $_GET['method'];
						if (isset($_GET['id'])) {
							$id = $_GET['id'];
						}
						switch ($method) {
							case 'delete_score':
								$delete_score = $this->model->deleteScore($id);
								if (!$delete_score) {
									echo " <script>location.href = 'index.php?page=score_search';</script>";
								}else {
									echo "<script> alert('Xóa không thành công!');</script>";
								}
								break;
							default:
								header("Location: index.php?page=score_search");
								break;
						}
		            }
					$student ="";
					$subject = "";
					$teacher = "";
					if ($_SERVER["REQUEST_METHOD"] == "GET") {	
						if (isset($_GET["student"])) {
							$student = trim($_GET["student"]);
						}
						if (isset($_GET["teacher"])) {
							$teacher = trim($_GET["teacher"]);
						}
						if (isset($_GET["subject"])) {
							$subject = trim($_GET["subject"]);
						}
						$students = $this->model->searchScores($student, $subject, $teacher);
					} else {
						$students = $this->model->getScore();
					}
					
					
					include_once "./app/view/".$page.".php";
					break;
				
				}
				
				
				
				
		}
	}

?>